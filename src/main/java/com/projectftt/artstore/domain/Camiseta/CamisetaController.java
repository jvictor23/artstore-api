package com.projectftt.artstore.domain.Camiseta;

import com.projectftt.artstore.core.controller.AbstractController;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/camiseta")
public class CamisetaController extends AbstractController<Camiseta> {
    
}
