package com.projectftt.artstore.domain.Camiseta;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.projectftt.artstore.domain.categoria.Categoria;

@Entity
@Table(name = "Camiseta")
public class Camiseta implements Serializable {
    private static final long serialVersionUID = 1L;


    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @Id
    private long id;

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @NotEmpty
    @NotBlank
    @Size(max = 120)
    @Column(name = "marca")
    private String marca;

    public String getMarca() {
        return this.marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    
    @NotNull
    @Column(name = "preco")
    private Double preco;

    public Double getPreco() {
        return this.preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    @NotEmpty
    @NotBlank
    @Size(max = 120)
    @Column(name = "cor")
    private String cor;

    public String getCor() {
        return this.cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    @NotEmpty
    @NotBlank
    @Size(max = 120)
    @Column(name = "tamanho")
    private String tamanho;

    public String getTamanho() {
        return this.tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho = tamanho;
    }


    @NotNull
    @JoinColumn(name = "categoria_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Categoria categoria;

    public Categoria getCategoria() {
        return this.categoria;
    }

    public void setCategoria(Categoria categoria){
        this.categoria = categoria;
    }

    @NotEmpty
    @NotBlank
    @Size(max = 120)
    @Column(name = "urlImage")
    private String urlImage;

    public String getUrlImage() {
        return this.urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

}
