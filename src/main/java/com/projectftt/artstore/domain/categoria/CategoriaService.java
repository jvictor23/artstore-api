package com.projectftt.artstore.domain.categoria;

import com.projectftt.artstore.core.service.AbstractService;

import org.springframework.stereotype.Service;

@Service
public class CategoriaService extends AbstractService<Categoria>{
    
}
