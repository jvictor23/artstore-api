package com.projectftt.artstore.domain.categoria;

import com.projectftt.artstore.core.controller.AbstractController;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/categoria")
public class CategoriaController extends AbstractController<Categoria> {
    
}
